// VS project 17.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <string>
#include <iostream>
#include <cmath>

enum class gender
{
    MALE,
    FEMALE,
    UNKWOWN
};

class Animal {
    unsigned int old;
    gender sex;
    std::string name;
public:
    Animal() {};
    Animal(std::string _name, int _old, gender _sex) : old(_old), sex(_sex), name( _name) {};

    void setOld(int _old) { old = _old; };
    int getOld() { return old; };
    
    void setSex(gender _sex) { sex = _sex; };
    gender getSex() { return sex; };

    void setName(std::string _name) { name = _name; };
    std::string getName() { return name; };

    void getFullInfo()
    {
        std::cout << "Base animal with next characteristic:\nName - " << getName() << "\nYears - " << getOld() << "\nSex - ";
        switch (getSex())
        {
        case gender::MALE:
            std::cout << "Male\n";
            break;
        case gender::FEMALE:
            std::cout << "Male\n";
            break;
        default:
            break;
        }
    };
};

struct Point {
    float x = 0;
    float y = 0;
};

class Vector {
    Point coordinate;
public:
    Vector() {};
    Vector(Point point) : coordinate(point) {};
    float module()
    {
        return std::sqrtf(coordinate.x*coordinate.x + coordinate.y*coordinate.y);
    }
    
};

int main()
{
    int i;
    Animal animal("Sharik",3,gender::MALE);
    animal.getFullInfo();
    Point xyVector;
    std::cout << "\n\n\n\n";
    std::cout << "Input coordinate vector: x = ";
    std::cin >> xyVector.x;
    std::cout << "Input coordinate vector: y = ";
    std::cin >> xyVector.y;
    Vector objVector(xyVector);
    std::cout << "Module vector: " << objVector.module();
    std::cin >> xyVector.y;
    
    return 0;
}

